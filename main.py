from aiohttp import web
from db import close_pg
from routes import setup_routes
import logging


app = web.Application()
logger = logging.getLogger()
logging.basicConfig(filename="logs.log", level=logging.DEBUG, format='%(asctime)s %(name)-14s %(levelname)s: %(message)s')
setup_routes(app)
app.on_cleanup.append(close_pg)
web.run_app(app, access_log_format='"%r" - %s - %D - %t - %b')
