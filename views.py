from db import util, asset1, cost, db, final_recommendation
from aiohttp import web
import json, aiohttp, asyncio, pymongo, logging, time, functools
from bson import json_util
from pymongo.errors import AutoReconnect


async def recommendation(req):
    try:
        ds_policy_util_ec2 = [
            {
                '$match': {
                    'metric': 'CPUUtilization'
                }
            }, {
                '$addFields': {
                    'minValue': {
                        '$toDouble': '$minValue'
                    }
                }
            }, {
                '$sort': {
                    'minValue': 1
                }
            }, {
                '$group': {
                    '_id': '$providerAssetId',
                    'listValues': {
                        '$push': '$minValue'
                    }
                }
            }, {
                '$project': {
                    'percentile': {
                        '$arrayElemAt': [
                            '$listValues', {
                                '$floor': {
                                    '$multiply': [
                                        0.95, {
                                            '$size': '$listValues'
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            }, {
                '$project': {
                    'ds_policy_util': {
                        '$cond': [
                            {
                                '$lt': [
                                    '$percentile', 0.8
                                ]
                            }, 'very Low', {
                                '$cond': [
                                    {
                                        '$and': [
                                            {
                                                '$gte': [
                                                    '$percentile', 0.8
                                                ]
                                            }, {
                                                '$lte': [
                                                    '$percentile', 3
                                                ]
                                            }
                                        ]
                                    }, 'Low', {
                                        '$cond': [
                                            {
                                                '$and': [
                                                    {
                                                        '$gte': [
                                                            '$percentile', 3
                                                        ]
                                                    }, {
                                                        '$lte': [
                                                            '$percentile', 10
                                                        ]
                                                    }
                                                ]
                                            }, 'Medium', 'High'
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                }
            },
            {
                '$merge': {
                    'into': {
                        'db': 'recommendation',
                        'coll': 'ds_policy_util_ec2'
                    }
                }
            }
        ]
        util.aggregate(ds_policy_util_ec2).to_list(length=None)
        # cursor.to_list(length=None)

        ds_policy_util_ebs = [
            {
                '$match': {
                    'metric': 'VolumeIdleTime'
                }
            }, {
                '$addFields': {
                    'minValue': {
                        '$toDouble': '$minValue'
                    }
                }
            }, {
                '$sort': {
                    'minValue': 1
                }
            }, {
                '$group': {
                    '_id': '$providerAssetId',
                    'listValues': {
                        '$push': '$minValue'
                    }
                }
            }, {
                '$project': {
                    'percentile': {
                        '$arrayElemAt': [
                            '$listValues', {
                                '$floor': {
                                    '$multiply': [
                                        0.05, {
                                            '$size': '$listValues'
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            }, {
                '$project': {
                    'ds_policy_util': {
                        '$cond': [
                            {
                                '$gt': [
                                    '$percentile', 299.5
                                ]
                            }, 'very Low', {
                                '$cond': [
                                    {
                                        '$and': [
                                            {
                                                '$gte': [
                                                    '$percentile', 199
                                                ]
                                            }, {
                                                '$lte': [
                                                    '$percentile', 299.5
                                                ]
                                            }
                                        ]
                                    }, 'Low', {
                                        '$cond': [
                                            {
                                                '$and': [
                                                    {
                                                        '$gte': [
                                                            '$percentile', 149
                                                        ]
                                                    }, {
                                                        '$lte': [
                                                            '$percentile', 199
                                                        ]
                                                    }
                                                ]
                                            }, 'Medium', 'High'
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                }
            }, {
                '$merge': {
                    'into': {
                        'db': 'recommendation',
                        'coll': 'ds_policy_util_ebs'
                    }
                }
            }
        ]
        util.aggregate(ds_policy_util_ebs).to_list(length=None)
        # cursor_1.to_list(length=None)

        avg_cost = [
            {
                '$addFields': {
                    'pcProviderTotalCost': {
                        '$toDouble': '$pcProviderTotalCost'
                    }
                }
            }, {
                '$group': {
                    '_id': '$providerAssetId',
                    'pcProviderAvgCost': {
                        '$avg': '$pcProviderTotalCost'
                    }
                }
            }, {
                '$merge': {
                    'into': {
                        'db': 'recommendation',
                        'coll': 'avg_cost'
                    }
                }
            }
        ]
        cost.aggregate(avg_cost).to_list(length=None)
        # cursor_2.to_list(length=None)

        await asyncio.sleep(1)

        ec2_ds_files = [
            {
                '$lookup': {
                    'from': 'ds_policy_util_ec2',
                    'localField': 'providerAssetId',
                    'foreignField': '_id',
                    'as': 'new_util'
                }
            }, {
                '$unwind': {
                    'path': '$new_util'
                }
            }, {
                '$unwind': {
                    'path': '$providerApiText'
                }
            }, {
                '$project': {
                    '_id': 0,
                    'providerAssetId': 1,
                    'ds_policy_util': '$new_util.ds_policy_util',
                    'ds_policy_tagStatus': {
                        '$cond': {
                            'if': {
                                '$eq': [
                                    '$tags', ''
                                ]
                            },
                            'then': 'Untagged',
                            'else': 'Tagged'
                        }
                    },
                    'ds_policy_monitorStatus': {
                        '$cond': {
                            'if': {
                                '$eq': [
                                    '$providerApiText.Monitoring.State', 'disabled'
                                ]
                            },
                            'then': 'Disabled',
                            'else': 'Enabled'
                        }
                    }
                }
            }, {
                '$lookup': {
                    'from': 'pre_recommendation_ec2',
                    'let': {
                        'util': '$ds_policy_util',
                        'tagStatus': '$ds_policy_tagStatus',
                        'monitorStatus': '$ds_policy_monitorStatus'
                    },
                    'pipeline': [
                        {
                            '$match': {
                                '$expr': {
                                    '$and': [
                                        {
                                            '$eq': [
                                                '$ds_policy_util', '$$util'
                                            ]
                                        }, {
                                            '$eq': [
                                                '$ds_policy_tagStatus', '$$tagStatus'
                                            ]
                                        }, {
                                            '$eq': [
                                                '$ds_policy_monitorStatus', '$$monitorStatus'
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    ],
                    'as': 'recommendation_ec2'
                }
            }, {
                '$unwind': {
                    'path': '$recommendation_ec2'
                }
            }, {
                '$lookup': {
                    'from': 'avg_cost',
                    'localField': 'providerAssetId',
                    'foreignField': '_id',
                    'as': 'cost'
                }
            }, {
                '$unwind': {
                    'path': '$cost'
                }
            }, {
                '$project': {
                    'providerAssetId': 1,
                    'ds_policy_util': 1,
                    'ds_policy_tagStatus': 1,
                    'ds_policy_monitorStatus': 1,
                    'ds_recom_priority': '$recommendation_ec2.ds_recom_priority',
                    'ds_recom_txt': '$recommendation_ec2.ds_recom_txt',
                    'ds_recom_reason': '$recommendation_ec2.ds_recom_reason',
                    'ds_recom_estcostsav': '$recommendation_ec2.ds_recom_estcostsav',
                    'ds_recom_estriskred': '$recommendation_ec2.ds_recom_estriskred',
                    'ds_recom_type': '$recommendation_ec2.ds_recom_type',
                    'pcProviderAvgCost': '$cost.pcProviderAvgCost'
                }
            }, {
                '$merge': {
                    'into': {
                        'db': 'recommendation',
                        'coll': 'final_recommendation'
                    }
                }
            }
        ]
        asset1.aggregate(ec2_ds_files).to_list(length=None)
        # cursor_3.to_list(length=None)

        ebs_ds_files = [
            {
                '$lookup': {
                    'from': 'ds_policy_util_ebs',
                    'localField': 'providerAssetId',
                    'foreignField': '_id',
                    'as': 'new_util'
                }
            }, {
                '$unwind': {
                    'path': '$new_util'
                }
            }, {
                '$project': {
                    '_id': 0,
                    'providerAssetId': 1,
                    'ds_policy_util': '$new_util.ds_policy_util',
                    'ds_policy_tagStatus': {
                        '$cond': {
                            'if': {
                                '$eq': [
                                    '$tags', ''
                                ]
                            },
                            'then': 'Untagged',
                            'else': 'Tagged'
                        }
                    },
                    'ds_policy_attachStatus': {
                        '$cond': {
                            'if': {
                                '$eq': [
                                    '$providerApiText.State', 'available'
                                ]
                            },
                            'then': 'Detached',
                            'else': 'Attached'
                        }
                    }
                }
            }, {
                '$lookup': {
                    'from': 'pre_recommendation_ebs',
                    'let': {
                        'util': '$ds_policy_util',
                        'tagStatus': '$ds_policy_tagStatus',
                        'attachStatus': '$ds_policy_attachStatus'
                    },
                    'pipeline': [
                        {
                            '$match': {
                                '$expr': {
                                    '$and': [
                                        {
                                            '$eq': [
                                                '$ds_policy_util', '$$util'
                                            ]
                                        }, {
                                            '$eq': [
                                                '$ds_policy_tagStatus', '$$tagStatus'
                                            ]
                                        }, {
                                            '$eq': [
                                                '$ds_policy_attachStatus', '$$attachStatus'
                                            ]
                                        }
                                    ]
                                }
                            }
                        }
                    ],
                    'as': 'recommendation_ebs'
                }
            }, {
                '$unwind': {
                    'path': '$recommendation_ebs'
                }
            }, {
                '$lookup': {
                    'from': 'avg_cost',
                    'localField': 'providerAssetId',
                    'foreignField': '_id',
                    'as': 'cost'
                }
            }, {
                '$unwind': {
                    'path': '$cost'
                }
            }, {
                '$project': {
                    'providerAssetId': 1,
                    'ds_policy_util': 1,
                    'ds_policy_tagStatus': 1,
                    'ds_policy_attachStatus': 1,
                    'ds_recom_priority': '$recommendation_ebs.ds_recom_priority',
                    'ds_recom_txt': '$recommendation_ebs.ds_recom_txt',
                    'ds_recom_reason': '$recommendation_ebs.ds_recom_reason',
                    'ds_recom_estcostsav': '$recommendation_ebs.ds_recom_estcostsav',
                    'ds_recom_estriskred': '$recommendation_ebs.ds_recom_estriskred',
                    'ds_recom_type': '$recommendation_ebs.ds_recom_type',
                    'pcProviderAvgCost': '$cost.pcProviderAvgCost'
                }
            }, {
                '$merge': {
                    'into': {
                        'db': 'recommendation',
                        'coll': 'final_recommendation'
                    }
                }
            }
        ]
        asset1.aggregate(ebs_ds_files).to_list(length=None)
        # cursor_4.to_list(length=None)

        response_obj = {'status': 'Successfully Created'}
        return web.Response(text=json.dumps(response_obj), status=201)

    except web.HTTPBadRequest as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=400)
    except web.HTTPNotFound as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=404)
    except web.HTTPRequestTimeout as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=408)
    except web.HTTPException as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=ex.status)
    except Exception as e:
        response_obj = {'status': 'failed', 'reason': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)


async def get_monitor_status(request):
    data = []
    try:
        async for doc in final_recommendation.find(
                {"ds_policy_monitorStatus": "{}".format(request.match_info['status'])}):
            data.append(doc)
        response_obj = json.loads(json_util.dumps(data))
        if len(response_obj) == 0:
            raise Exception("Collection is empty")
        else:
            return web.Response(text=json.dumps(response_obj), status=200)
    except web.HTTPBadRequest as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=400)
    except web.HTTPNotFound as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=404)
    except web.HTTPRequestTimeout as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=408)
    except web.HTTPException as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=ex.status)
    except Exception as e:
        response_obj = {'status': 'failed', 'reason': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)


async def get_tag_status(request):
    data = []
    try:
        async for doc in final_recommendation.find({"ds_policy_tagStatus": "{}".format(request.match_info['tag'])}):
            data.append(doc)
        response_obj = json.loads(json_util.dumps(data))
        if len(response_obj) == 0:
            raise Exception("Collection is empty")
        else:
            return web.Response(text=json.dumps(response_obj), status=200)
    except web.HTTPBadRequest as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=400)
    except web.HTTPNotFound as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=404)
    except web.HTTPRequestTimeout as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=408)
    except RuntimeError as ex:
        response_obj = {'status': 'failed', 'reason': ex}
        return web.Response(text=json.dumps(response_obj), status=500)
    except TypeError as e:
        response_obj = {'status': 'failed', 'reason': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)


async def top_recommendation(req):
    data = []
    try:
        async for doc in final_recommendation.find().sort([('pcProviderAvgCost', -1)]).limit(10):
            data.append(doc)
        response_obj = json.loads(json_util.dumps(data))
        if len(response_obj) == 0:
            raise Exception("Collection is empty")
        else:
            return web.Response(text=json.dumps(response_obj), status=200)
    except web.HTTPBadRequest as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=400)
    except web.HTTPNotFound as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=404)
    except web.HTTPRequestTimeout as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=408)
    except web.HTTPException as ex:
        response_obj = {'status': 'failed', 'reason': str(ex)}
        return web.Response(text=json.dumps(response_obj), status=ex.status)
    except Exception as e:
        response_obj = {'status': 'failed', 'reason': str(e)}
        return web.Response(text=json.dumps(response_obj), status=500)
























































# async def top_recomm(request):
#     try:
#         cursor = final_recommendation.find().sort([('pcProviderAvgCost', -1)]).limit(10)
#         docs = await cursor.to_list(length=None)
#         while docs:
#             p.pprint(docs)
#             docs = await cursor.to_list(length=None)
#             response_obj = json.loads(json_util.dumps(docs))
#             return web.Response(text=json.dumps(response_obj), status=200)
#     except Exception as exc:
#         print("Error:", exc)
#


# loop = asyncio.get_event_loop()
# loop.run_until_complete(final_recomm())


# documents = list(asset.find({}, {"_id": 0, "providerApiText": 1, "providerAssetId": 1}))
# documents1 = list(asset1.find({}, {"_id": 0, "providerApiText": 1, "providerAssetId": 1}))
# for i in documents:
#     for j in documents1:
#         if i["providerAssetId"] == j["providerAssetId"]:
#             y = i["providerApiText"]
#             # p.pprint(y)
#             json_docs = json.loads(y)
#             print("MongoDB collections:", json_docs)
#             print("##################################################################")
#             asset1.update_many({"providerAssetId": i["providerAssetId"]}, {"$set": {"providerApiText": json_docs}})
#             print("555555555555555555555555555555555555555555555555555555555555555555555555555555555555")
#             asset1.save(json_docs)
# print("updated")
