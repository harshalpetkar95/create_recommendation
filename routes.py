from views import recommendation, get_monitor_status, top_recommendation, get_tag_status


def setup_routes(app):
    app.router.add_get('/top_recommendation', top_recommendation),
    app.router.add_get('/create_recommendation', recommendation),
    app.router.add_get('/tag_status/{tag}', get_tag_status),
    app.router.add_get('/monitor_status/{status}', get_monitor_status)
