import motor.motor_asyncio, time
from pymongo.errors import AutoReconnect
import json, asyncio, aiohttp

try:
    client = motor.motor_asyncio.AsyncIOMotorClient(
        "mongodb+srv://Hitman:<>@cluster0.8yx9l.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
    db = client["recommendation"]
    util = db["util"]
    asset1 = db["asset1"]
    test = ["test"]
    cost = db["cost"]
    final_recommendation = db["final_recommendation"]
    print("Connected successfully!!!")
except AutoReconnect as e:
    print("Error:", e)
except Exception as exp:
    print("Could not connect to MongoDB")
    print("Error:", exp)


async def close_pg(app):
    app[db].close()
    await app[db].wait_closed()


# class Mongo_connection():
#     def __init__(self):
#         self.util = None
#         self.asset1 = None
#         self.cost = None
#         self.test = None
#         self.pre_recommendation_ec2 = None
#         self.pre_recommendation_ebs = None
#
#     def mongo(self):
#         try:
#             client = motor.motor_asyncio.AsyncIOMotorClient(
#                 "mongodb+srv://Hitman:Hitman@cluster0.8yx9l.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
#             db = client["recommendation"]
#             self.util = db["util"]
#             self.asset1 = db["asset1"]
#             self.cost = db["cost"]
#             self.test = db["test"]
#             self.pre_recommendation_ebs = db["pre_recommendation_ebs"]
#             self.pre_recommendation_ec2 = db["pre_recommendation_ec2"]
#             print("Connected successfully!!!")
#         except Exception as exp:
#             print("Could not connect to MongoDB")
#             print("Error:", exp)
